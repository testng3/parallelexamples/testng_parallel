## 1. Przypadek pierwszy: _**parallel=tests**_

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "https://testng.org/testng-1.0.dtd" >
<suite name="TestNG - Parallel=tests" parallel="tests" thread-count="2">
    <test name="First" >
        <classes>
            <class name="pl.automaty.testng.tests.LoginTest"/>
            <class name="pl.automaty.testng.tests.CreateUserTest"/>
        </classes>
    </test>
    <test name="Second" >
        <classes>
            <class name="pl.automaty.testng.tests.SearchProductTest"/>
            <class name="pl.automaty.testng.tests.LogoutTest"/>
        </classes>
    </test>
</suite>
```


**parallel="tests** - TestNG uruchomi wszystkie metody zawarte w tym samym tagu `<test>` w tym samym wątku, ale wszystkie tagi `<test>` będą w oddzielnych wątkach.
W tym przypadku parametr **parallel** mamy wpisany w tagu `<suite>` i ustawioną wartość **tests** a ilość wątków na dwa.  

```
SearchProductTest -> searchEmptyProduct 	:	22
LoginTest -> loginExistingUser 	:	21
LoginTest -> loginNonExistingUser 	:	21
SearchProductTest -> searchExistingProduct 	:	22
SearchProductTest -> searchInvalidExistingProduct 	:	22
CreateUserTest -> createUserWithCorrectData 	:	21
SearchProductTest -> searchNonExistingProduct 	:	22
CreateUserTest -> createUserWithEmptyData 	:	21
CreateUserTest -> createUserWithIncorrectData 	:	21
LogoutTest -> logoutWhenUserIsLogin 	:	22
LogoutTest -> logoutWhenUserIsLogout 	:	22

===============================================
TestNG - Parallel=tests
Total tests run: 11, Passes: 11, Failures: 0, Skips: 0
===============================================


Process finished with exit code 0
```


W pierwszyn zestawie testów `<test name="First">` dla klasy **LoginTest** przypisany został wątek o id 21, dla klasy **CreateUserTest** też wątek o id 21. W drugim zestawie testów `<test name="Second">` analogicznie dla klasy **SearchProductTest** przypisany został wątek o id 22, a dla klasy **LogoutTest** także wątek o id 22.


Co się stanie jeśli ustawienia wątków przeniesiemy z tagu `<suite>` do tagu `<test name="First" parallel="tests" thread-count="2">`? Nie przyniesie to żadnego efektu w postaci wykonywania testów współbierznie, wszytskie testy wykonane zostaną sekwencyjnie:

```
LoginTest -> loginExistingUser 	:	1
LoginTest -> loginNonExistingUser 	:	1
CreateUserTest -> createUserWithCorrectData 	:	1
CreateUserTest -> createUserWithEmptyData 	:	1
CreateUserTest -> createUserWithIncorrectData 	:	1
SearchProductTest -> searchEmptyProduct 	:	1
SearchProductTest -> searchExistingProduct 	:	1
SearchProductTest -> searchInvalidExistingProduct 	:	1
SearchProductTest -> searchNonExistingProduct 	:	1
LogoutTest -> logoutWhenUserIsLogin 	:	1
LogoutTest -> logoutWhenUserIsLogout 	:	1

===============================================
TestNG - Parallel=tests
Total tests run: 11, Passes: 11, Failures: 0, Skips: 0
===============================================


Process finished with exit code 0
```

## 2. Przypadek drugi: _**parallel=classes**_

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "https://testng.org/testng-1.0.dtd" >
<suite name="TestNG - Parallel=classes" parallel="classes" thread-count="4">
    <test name="First" >
        <classes>
            <class name="pl.automaty.testng.tests.LoginTest"/>
            <class name="pl.automaty.testng.tests.CreateUserTest"/>
            <class name="pl.automaty.testng.tests.SearchProductTest"/>
            <class name="pl.automaty.testng.tests.LogoutTest"/>
        </classes>
    </test>
</suite>
```

**parallel="classes** - TestNG uruchomi wszystkie metody w tej samej klasie w tym samym wątku, ale poszczególne klasy będą działały w ramach różnych wątków.
W tym przypadku ustawiamy wartość **classes** dla parametru **parallel** w tagu `<suite>`, ilość wątków ustawiliśmy na cztery. Klasy testowe mamy zgrupowane w jednym tagu `<test>`. Ustawienie to powaduje utworzenie czterech wątków po jednym dla każdej klasy testowej. Gdy by wątków było mniej niż klas testowych, dwie lub więcej klas testowych obsłużył by ten sam wątek.

Jak widać poniżej, każda klasa testowa otrzymała swój wątek.

```
CreateUserTest -> createUserWithCorrectData 	:	24
LogoutTest -> logoutWhenUserIsLogin 	:	26
SearchProductTest -> searchEmptyProduct 	:	25
LoginTest -> loginExistingUser 	:	23
SearchProductTest -> searchExistingProduct 	:	25
LoginTest -> loginNonExistingUser 	:	23
CreateUserTest -> createUserWithEmptyData 	:	24
SearchProductTest -> searchInvalidExistingProduct 	:	25
LogoutTest -> logoutWhenUserIsLogout 	:	26
CreateUserTest -> createUserWithIncorrectData 	:	24
SearchProductTest -> searchNonExistingProduct 	:	25

===============================================
TestNG - Parallel=classes
Total tests run: 11, Passes: 11, Failures: 0, Skips: 0
===============================================


Process finished with exit code 0
```
W kolejnym kroku klasy testowe rozdzielamy między dwa tagi `<test>`, pozostałe ustawienia zostają nie zmienione. 

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "https://testng.org/testng-1.0.dtd" >
<suite name="TestNG - Parallel=classes"  parallel="classes" thread-count="4" >
    <test name="First">
        <classes>
            <class name="pl.automaty.testng.tests.LoginTest"/>
            <class name="pl.automaty.testng.tests.CreateUserTest"/>
        </classes>
    </test>
    <test name="Second">
        <classes>
            <class name="pl.automaty.testng.tests.SearchProductTest"/>
            <class name="pl.automaty.testng.tests.LogoutTest"/>
        </classes>
    </test>
</suite>
```

Teraz jako pierwsze wykonają się wielowątkowo klasy zawarte w pierwszym tagu `<test name="First">`, a po nich wykonają sie wielowątkowo klasy z tagu `<test name="Second">`. 

Wynik poniżej:
```
LoginTest -> loginExistingUser 	:	22
CreateUserTest -> createUserWithCorrectData 	:	23
LoginTest -> loginNonExistingUser 	:	22
CreateUserTest -> createUserWithEmptyData 	:	23
CreateUserTest -> createUserWithIncorrectData 	:	23
LogoutTest -> logoutWhenUserIsLogin 	:	25
SearchProductTest -> searchEmptyProduct 	:	24
LogoutTest -> logoutWhenUserIsLogout 	:	25
SearchProductTest -> searchExistingProduct 	:	24
SearchProductTest -> searchInvalidExistingProduct 	:	24
SearchProductTest -> searchNonExistingProduct 	:	24

===============================================
TestNG - Parallel=classes
Total tests run: 11, Passes: 11, Failures: 0, Skips: 0
===============================================


Process finished with exit code 0
```
Kolejna zmiana - tym razem przenosimy konfigurację wątków z tagu `<suite>` do tagu `<test>`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "https://testng.org/testng-1.0.dtd" >
<suite name="TestNG - Parallel=classes">
    <test name="First"  parallel="classes" thread-count="2" >
        <classes>
            <class name="pl.automaty.testng.tests.LoginTest"/>
            <class name="pl.automaty.testng.tests.CreateUserTest"/>
        </classes>
    </test>
    <test name="Second">
        <classes>
            <class name="pl.automaty.testng.tests.SearchProductTest"/>
            <class name="pl.automaty.testng.tests.LogoutTest"/>
        </classes>
    </test>
</suite>
```

Jak widać w wyniku testy ujęte w tagu `<test name="First"  parallel="classes" thread-count="2">` wykonają się współbierznie, natomiast testy z tagu `<test name="Second">` wykonają się sekwencyjnie.

```
CreateUserTest -> createUserWithCorrectData 	:	22
LoginTest -> loginExistingUser 	:	21
LoginTest -> loginNonExistingUser 	:	21
CreateUserTest -> createUserWithEmptyData 	:	22
CreateUserTest -> createUserWithIncorrectData 	:	22
SearchProductTest -> searchEmptyProduct 	:	1
SearchProductTest -> searchExistingProduct 	:	1
SearchProductTest -> searchInvalidExistingProduct 	:	1
SearchProductTest -> searchNonExistingProduct 	:	1
LogoutTest -> logoutWhenUserIsLogin 	:	1
LogoutTest -> logoutWhenUserIsLogout 	:	1

===============================================
TestNG - Parallel=classes
Total tests run: 11, Passes: 11, Failures: 0, Skips: 0
===============================================


Process finished with exit code 0
```
## Przypadek trzeci: _**parallel=methods**_

**parallel="methods** - TestNG uruchomi wszystkie metody testowe w oddzielnych wątkach.
Tym razem parametr **parallel** ma wartość **methods** ustawioną w tagu `<suite>`, klasy testowe są zgrupowane w ramach jednego tagu `<test>` a ilość wątków ustawiona jest na cztery.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "https://testng.org/testng-1.0.dtd" >
<suite name="TestNG - Parallel=methods"  parallel="methods" thread-count="4" >
    <test name="First">
        <classes>
            <class name="pl.automaty.testng.tests.LoginTest"/>
            <class name="pl.automaty.testng.tests.CreateUserTest"/>
            <class name="pl.automaty.testng.tests.SearchProductTest"/>
            <class name="pl.automaty.testng.tests.LogoutTest"/>
        </classes>
    </test>
</suite>
```

Ustawienie to spowoduje przypisanie wątków do metod a nie klas testowych. W tym przypadku wątek o ID 25 wykonuje metody z klas **CreateUserTest**, **SearchProductTest** i **LogoutTest**.

```
CreateUserTest -> createUserWithCorrectData     :   25
CreateUserTest -> createUserWithEmptyData   :   26
LoginTest -> loginNonExistingUser   :   24
LoginTest -> loginExistingUser  :   23
CreateUserTest -> createUserWithIncorrectData   :   23
SearchProductTest -> searchEmptyProduct     :   26
SearchProductTest -> searchExistingProduct  :   25
SearchProductTest -> searchNonExistingProduct   :   23
SearchProductTest -> searchInvalidExistingProduct   :   24
LogoutTest -> logoutWhenUserIsLogin     :   26
LogoutTest -> logoutWhenUserIsLogout    :   25

===============================================
TestNG - Parallel=methods
Total tests run: 11, Passes: 11, Failures: 0, Skips: 0
===============================================


Process finished with exit code 0
```
W następnym przykładzie rozdzielamy klasy testowe pomiędzy dwa tagi `<test>` i zmniejszamy ilość wątków z czterech do dwóch.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "https://testng.org/testng-1.0.dtd" >
<suite name="TestNG - Parallel=methods"  parallel="methods" thread-count="2" >
    <test name="First">
        <classes>
            <class name="pl.automaty.testng.tests.LoginTest"/>
            <class name="pl.automaty.testng.tests.CreateUserTest"/>
        </classes>
    </test>
    <test name="Second">
        <classes>
            <class name="pl.automaty.testng.tests.SearchProductTest"/>
            <class name="pl.automaty.testng.tests.LogoutTest"/>
        </classes>
    </test>
</suite>
```
W wyniku tych zmian klasy testowe zawarte w tagu `<test name="First">` i w tagu `<test name="Second">` otrzymają po dwa różne wątki przypisane do metod, to znaczy że jeden wątek może wykonywać metody testowe z różnych klas testowych zawartych w tym samym tagu `<test>`. Ponizszy wynik powinien wyjaśnić sytuację:

```
LoginTest -> loginNonExistingUser   :   24
LoginTest -> loginExistingUser  :   23
CreateUserTest -> createUserWithCorrectData     :   24
CreateUserTest -> createUserWithEmptyData   :   23
CreateUserTest -> createUserWithIncorrectData   :   24
SearchProductTest -> searchExistingProduct  :   26
SearchProductTest -> searchInvalidExistingProduct   :   26
SearchProductTest -> searchNonExistingProduct   :   26
LogoutTest -> logoutWhenUserIsLogin     :   25
LogoutTest -> logoutWhenUserIsLogout    :   26
SearchProductTest -> searchEmptyProduct     :   25

===============================================
TestNG - Parallel=methods
Total tests run: 11, Passes: 11, Failures: 0, Skips: 0
===============================================


Process finished with exit code 0
```
Gdy ustawienia wątków przeniesiemy z tagu `<suite>` do tagu `<test>` w ten sposób: `<test name="First"  parallel="methods" thread-count="2">`.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "https://testng.org/testng-1.0.dtd" >
<suite name="TestNG - Parallel=methods">
    <test name="First"  parallel="methods" thread-count="2">
        <classes>
            <class name="pl.automaty.testng.tests.LoginTest"/>
            <class name="pl.automaty.testng.tests.CreateUserTest"/>
        </classes>
    </test>
    <test name="Second">
        <classes>
            <class name="pl.automaty.testng.tests.SearchProductTest"/>
            <class name="pl.automaty.testng.tests.LogoutTest"/>
        </classes>
    </test>
</suite>
```

Spowaduje to że metody z tagu `<test>` z ustawionym parametrem **parallel** wykonają się współbierznie a metody z tagu `<test name="Second">` wykonają się sekwencyjnie.
Wynik:
```
LoginTest -> loginNonExistingUser   :   22
LoginTest -> loginExistingUser  :   21
CreateUserTest -> createUserWithCorrectData     :   22
CreateUserTest -> createUserWithEmptyData   :   21
CreateUserTest -> createUserWithIncorrectData   :   22
SearchProductTest -> searchEmptyProduct     :   1
SearchProductTest -> searchExistingProduct  :   1
SearchProductTest -> searchInvalidExistingProduct   :   1
SearchProductTest -> searchNonExistingProduct   :   1
LogoutTest -> logoutWhenUserIsLogin     :   1
LogoutTest -> logoutWhenUserIsLogout    :   1

===============================================
TestNG - Parallel=methods
Total tests run: 11, Passes: 11, Failures: 0, Skips: 0
===============================================


Process finished with exit code 0
```
